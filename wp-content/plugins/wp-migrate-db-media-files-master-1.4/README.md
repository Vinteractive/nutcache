#WP Migrate DB Media Files
An addon for [WP Migrate DB](https://github.com/slang800/wp-migrate-db) that lets you sync media libraries between WordPress installations.

<p align="center"><a><img src="https://raw.github.com/slang800/psychic-ninja/master/wp-migrate-db-media-files.png"/></a></p>
